/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.simpleprefs;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

public class MainActivity extends AppCompatActivity {
  private NavController nav;
  private AppBarConfiguration appBarCfg;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    nav = ((NavHostFragment)getSupportFragmentManager()
      .findFragmentById(R.id.nav_host))
      .getNavController();
    appBarCfg = new AppBarConfiguration.Builder(nav.getGraph()).build();

    NavigationUI.setupActionBarWithNavController(this, nav, appBarCfg);
  }

  @Override
  public boolean onSupportNavigateUp() {
    return NavigationUI.navigateUp(nav, appBarCfg) ||
      super.onSupportNavigateUp();
  }
}
