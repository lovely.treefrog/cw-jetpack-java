/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.formwidgets;

import android.os.Bundle;
import android.util.Log;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import com.commonsware.jetpack.samplerj.formwidgets.databinding.ActivityMainBinding;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
  private static final String TAG = "FormWidgets";
  private ActivityMainBinding binding;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    binding = ActivityMainBinding.inflate(getLayoutInflater());
    setContentView(binding.getRoot());

    binding.icon.setOnClickListener(v -> log(R.string.icon_clicked));
    binding.button.setOnClickListener(v -> log(R.string.button_clicked));

    binding.swytch.setOnCheckedChangeListener((v, isChecked) ->
      log(isChecked ? R.string.switch_checked : R.string.switch_unchecked));

    binding.checkbox.setOnCheckedChangeListener((v, isChecked) ->
      log(isChecked ? R.string.checkbox_checked : R.string.checkbox_unchecked));

    binding.radioGroup.setOnCheckedChangeListener(this::onRadioGroupChange);

    binding.seekbar.setOnSeekBarChangeListener(
      new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress,
                                      boolean fromUser) {
          String msg = getString(R.string.seekbar_changed, progress);

          binding.log.setText(msg);
          Log.d(TAG, msg);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
          // ignored
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
          // ignored
        }
      });
  }

  private void onRadioGroupChange(RadioGroup group, int checkedId) {
    @StringRes int msg;

    if (checkedId == R.id.radioButton1) {
      msg = R.string.radiobutton1_checked;
    }
    else if (checkedId == R.id.radioButton2) {
      msg = R.string.radiobutton2_checked;
    }
    else {
      msg = R.string.radiobutton3_checked;
    }

    log(msg);
  }

  private void log(@StringRes int msg) {
    binding.log.setText(msg);
    Log.d(TAG, getString(msg));
  }
}
