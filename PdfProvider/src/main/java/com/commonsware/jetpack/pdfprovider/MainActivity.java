/*
  Copyright (c) 2019-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.pdfprovider;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.commonsware.jetpack.pdfprovider.databinding.ActivityMainBinding;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.lifecycle.ViewModelProvider;

public class MainActivity extends AppCompatActivity {
  private static final String AUTHORITY =
    BuildConfig.APPLICATION_ID + ".provider";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    final ActivityMainBinding binding =
      ActivityMainBinding.inflate(getLayoutInflater());

    setContentView(binding.getRoot());

    final MainMotor motor = new ViewModelProvider(this).get(MainMotor.class);

    motor.getStates().observe(this, state -> {
      binding.export.setEnabled(!state.isLoading && state.content == null);
      binding.view.setEnabled(!state.isLoading && state.content != null);

      if (binding.view.isEnabled()) {
        binding.view.setOnClickListener(v -> {
          Uri uri = FileProvider.getUriForFile(this, AUTHORITY, state.content);
          Intent intent =
            new Intent(Intent.ACTION_VIEW)
              .setDataAndType(uri, "application/pdf")
              .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

          try {
            startActivity(intent);
          }
          catch (ActivityNotFoundException ex) {
            Toast.makeText(this, "Sorry, we cannot display that PDF!",
              Toast.LENGTH_LONG).show();
          }
        });
      }

      if (state.error != null) {
        binding.error.setText(state.error.getLocalizedMessage());
      }
    });

    binding.export.setOnClickListener(v -> motor.exportPdf());
  }
}
