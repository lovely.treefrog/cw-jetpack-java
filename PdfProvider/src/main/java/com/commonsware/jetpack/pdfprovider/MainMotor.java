/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.pdfprovider;

import android.app.Application;
import android.content.res.AssetManager;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class MainMotor extends AndroidViewModel {
  private static final String FILENAME = "test.pdf";
  private final MutableLiveData<MainViewState> states = new MutableLiveData<>();
  private final AssetManager assets;
  private final File dest;
  private final Executor executor = Executors.newSingleThreadExecutor();

  public MainMotor(@NonNull Application application) {
    super(application);

    assets = application.getAssets();
    dest = new File(application.getFilesDir(), FILENAME);

    if (dest.exists()) {
      states.setValue(new MainViewState(false, dest, null));
    }
  }

  LiveData<MainViewState> getStates() {
    return states;
  }

  void exportPdf() {
    states.setValue(new MainViewState(true, null, null));

    executor.execute(() -> {
      try {
        copy(assets.open(FILENAME));
        states.postValue(new MainViewState(false, dest, null));
      }
      catch (IOException e) {
        states.postValue(new MainViewState(false, null, e));
      }
    });
  }

  private void copy(InputStream in) throws IOException {
    FileOutputStream out=new FileOutputStream(dest);
    byte[] buf=new byte[8192];
    int len;

    while ((len=in.read(buf)) > 0) {
      out.write(buf, 0, len);
    }

    in.close();
    out.getFD().sync();
    out.close();
  }
}
