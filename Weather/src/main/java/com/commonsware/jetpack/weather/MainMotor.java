/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.weather;

import android.app.Application;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

public class MainMotor extends AndroidViewModel {
  final private WeatherRepository repo = WeatherRepository.get();
  final MediatorLiveData<MainViewState> results = new MediatorLiveData<>();
  private LiveData<WeatherResult> lastResult;

  public MainMotor(@NonNull Application application) {
    super(application);
  }

  void load(String office, int gridX, int gridY) {
    if (lastResult != null) {
      results.removeSource(lastResult);
    }

    lastResult = repo.load(office, gridX, gridY);
    results.addSource(lastResult, weather -> {
      ArrayList<RowState> rows = new ArrayList<>();

      if (weather.forecasts != null) {
        for (Forecast forecast : weather.forecasts) {
          String temp =
            getApplication().getString(R.string.temp, forecast.temperature,
              forecast.temperatureUnit);

          rows.add(new RowState(forecast.name, temp, forecast.icon));
        }
      }

      results.postValue(
        new MainViewState(weather.isLoading, rows, weather.error));
    });
  }
}
