/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.bookmarker;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.commonsware.jetpack.bookmarker.databinding.RowBinding;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ListAdapter;

public class BookmarkAdapter extends ListAdapter<RowState, RowHolder> {
  final private LayoutInflater inflater;

  BookmarkAdapter(LayoutInflater inflater) {
    super(RowState.DIFFER);
    this.inflater = inflater;
  }

  @NonNull
  @Override
  public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    RowBinding binding = RowBinding.inflate(inflater, parent, false);

    return new RowHolder(binding);
  }

  @Override
  public void onBindViewHolder(@NonNull RowHolder holder, int position) {
    holder.bind(getItem(position));
  }
}
