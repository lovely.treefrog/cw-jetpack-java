/*
  Copyright (c) 2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.commonsware.jetpack.samplerj.dialog.databinding.FragmentMainBinding;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

public class MainFragment extends Fragment {
  private FragmentMainBinding binding;

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater,
                           @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    binding = FragmentMainBinding.inflate(inflater, container, false);

    return binding.getRoot();
  }

  @Override
  public void onViewCreated(@NonNull View view,
                            @Nullable Bundle savedInstanceState) {
    binding.nuke.setOnClickListener(v ->
      NavHostFragment.findNavController(MainFragment.this)
        .navigate(R.id.confirmNuke));

    final NavController navController = NavHostFragment.findNavController(this);
    final ViewModelProvider viewModelProvider = new ViewModelProvider(
      navController.getViewModelStoreOwner(R.id.nav_graph));
    final GraphViewModel vm = viewModelProvider.get(GraphViewModel.class);

    vm.getResultStream().observe(getViewLifecycleOwner(),
      new Event.EventObserver<>(wasAccepted -> {
        if (wasAccepted) {
          Toast.makeText(requireContext(), "BOOOOOOOM!",
            Toast.LENGTH_LONG).show();
        }
      }));
  }
}
