## Elements of Android Jetpack

[*Elements of Android Jetpack*](https://commonsware.com/Jetpack/)
is an introductory book on Android app development, focusing on second-generation
Android development techniques. In particular, the book covers Jetpack, Google's
recommended set of libraries for creating Android apps.

This book is updated a few times a year and is available through
[the Warescription](https://commonsware.com/warescription) program. Subscribers also have access to office
hours chats and other benefits.

The book includes sample code in both Java and Kotlin. This repository contains
the primary Java sample apps.

This repository contains the source code for the sample apps profiled in the book. These 
samples are updated as the book is, with `git` tags applied to tie sample code versions to book
versions.

The book, and the samples, were written by Mark Murphy. You may also have run into him through
Stack Overflow:

<a href="https://stackoverflow.com/users/115145/commonsware">
<img src="https://stackoverflow.com/users/flair/115145.png" width="208" height="58" alt="profile for CommonsWare at Stack Overflow, Q&amp;A for professional and enthusiast programmers" title="profile for CommonsWare at Stack Overflow, Q&amp;A for professional and enthusiast programmers">
</a>

All of the source code in this archive is licensed under the
Apache 2.0 license except as noted.
