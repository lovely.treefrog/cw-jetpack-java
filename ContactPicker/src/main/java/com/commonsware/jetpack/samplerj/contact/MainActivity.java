/*
  Copyright (c) 2018-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.contact;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.widget.Toast;
import com.commonsware.jetpack.samplerj.contact.databinding.ActivityMainBinding;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

public class MainActivity extends AppCompatActivity {
  private static final int REQUEST_PICK = 1337;
  private ContactViewModel vm;
  private ActivityMainBinding binding;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    binding = ActivityMainBinding.inflate(getLayoutInflater());
    setContentView(binding.getRoot());

    vm = new ViewModelProvider(this).get(ContactViewModel.class);

    updateViewButton();

    binding.pick.setOnClickListener(v -> {
        try {
          startActivityForResult(new Intent(Intent.ACTION_PICK,
            ContactsContract.Contacts.CONTENT_URI), REQUEST_PICK);
        }
        catch (Exception e) {
          Toast.makeText(this, R.string.msg_pick_error,
            Toast.LENGTH_LONG).show();
        }
      }
    );

    binding.view.setOnClickListener(
      v -> {
        try {
          startActivity(new Intent(Intent.ACTION_VIEW, vm.getContact()));
        }
        catch (Exception e) {
          Toast.makeText(this, R.string.msg_view_error,
            Toast.LENGTH_LONG).show();
        }
      });
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode,
                                  @Nullable Intent data) {
    if (requestCode == REQUEST_PICK) {
      if (resultCode == RESULT_OK &&
        data != null) {
        vm.setContact(data.getData());
        updateViewButton();
      }
    }
    else {
      super.onActivityResult(requestCode, resultCode, data);
    }
  }

  private void updateViewButton() {
    if (vm.getContact() != null) {
      binding.view.setEnabled(true);
    }
  }
}
