/*
  Copyright (c) 2019-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.livedata;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.commonsware.jetpack.samplerj.livedata.databinding.FragmentListBinding;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ColorListFragment extends Fragment {
  private FragmentListBinding binding;

  @Override
  public View onCreateView(@NonNull LayoutInflater inflater,
                           ViewGroup container,
                           Bundle savedInstanceState) {
    binding = FragmentListBinding.inflate(inflater, container, false);

    return binding.getRoot();
  }

  @Override
  public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
    ColorViewModel vm = new ViewModelProvider(this).get(ColorViewModel.class);

    ColorAdapter colorAdapter = new ColorAdapter(getLayoutInflater(),
      this::navTo);

    binding.items.setLayoutManager(new LinearLayoutManager(requireContext()));
    binding.items.addItemDecoration(new DividerItemDecoration(requireContext(),
      DividerItemDecoration.VERTICAL));
    binding.items.setAdapter(colorAdapter);

    vm.numbers.observe(getViewLifecycleOwner(), colorAdapter::submitList);
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();

    binding = null;
  }

  private void navTo(int color) {
    NavHostFragment.findNavController(this)
      .navigate(ColorListFragmentDirections.showColor(color));
  }
}