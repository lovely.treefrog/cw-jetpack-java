/*
  Copyright (c) 2017-2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.work.download;

import android.content.Context;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import java.io.File;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;
import androidx.work.testing.WorkManagerTestInitHelper;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class DownloadWorkerTest {
  private File expected;
  private final Context context =
    InstrumentationRegistry.getInstrumentation().getTargetContext();

  @Before
  public void setUp() {
    WorkManagerTestInitHelper.initializeTestWorkManager(context);

    expected = new File(context.getCacheDir(), "oldbook.pdf");

    if (expected.exists()) {
      expected.delete();
    }
  }

  @Test
  public void download() {
    assertFalse(expected.exists());

    WorkManager.getInstance(context).enqueue(buildWorkRequest(null));

    assertTrue(expected.exists());
  }

  @Test
  public void downloadWithConstraints() {
    Constraints constraints = new Constraints.Builder()
      .setRequiredNetworkType(NetworkType.CONNECTED)
      .setRequiresBatteryNotLow(true)
      .build();
    WorkRequest work = buildWorkRequest(constraints);

    assertFalse(expected.exists());

    WorkManager.getInstance(context).enqueue(work);
    WorkManagerTestInitHelper.getTestDriver(context)
      .setAllConstraintsMet(work.getId());

    assertTrue(expected.exists());
  }

  private WorkRequest buildWorkRequest(Constraints constraints) {
    OneTimeWorkRequest.Builder builder =
      new OneTimeWorkRequest.Builder(DownloadWorker.class)
        .setInputData(new Data.Builder()
          .putString(DownloadWorker.KEY_URL,
            "https://commonsware.com/Android/Android-1_0-CC.pdf")
          .putString(DownloadWorker.KEY_FILENAME, "oldbook.pdf")
          .build())
        .addTag("download");

    if (constraints != null) {
      builder.setConstraints(constraints);
    }

    return builder.build();
  }
}
